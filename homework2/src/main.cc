/* -------------------------------------------------------------------------- */
#include <cstdlib>
#include <iostream>
#include <memory>
#include "compute_arithmetic.hh"
#include "compute_pi.hh"
#include "print_series.hh"
#include "write_series.hh"

/* -------------------------------------------------------------------------- */

static void show_usage(std::string name)
{
    std::cerr << "Usage: " << name << " series_type output_type <option(s)>\n"
              << "\t series_type \t 'pi' or 'arithmetic'\n"
              << "\t output_type \t 'print' or 'write' (i.e write to file)\n"
              << "Options:\n"
              << "\t-h,--help \tShow this help message\n"
              << "\t-f,--freq \tFrequency of the output. Default: 1\n"
              << "\t-m,--maxiter \tMaximum number of terms in the series. Default: 100\n"
              << "\t-s,--separator \tSeparator used when writing the output.\n"
              << "\t\t\tChoose among {\" \", \"space\", \"\\t\", \"tab\",\n"
                 "\t\t\t\",\", \"comma\",\"|\", \"pipe\"}\n"
                 "\t\t\tDefault: \" \"\n"
              << "\t-f,--filename \tName of file where output will be printed/written.\n"
              << "\t-p,--precision \tPrecision of values to be printed/written.\n"
              << "\t\t\tExtension determined automatically based on separator.\n"
              << "\t\t\tDefault: 'output'\n"
              << "\n"
              << "Example: from the executable location, use command:\n"
              << "./main pi write --maxiter 100 --freq 10 --filename results --precision 5 --separator \",\"\n";

}

/* -------------------------------------------------------------------------- */

int main(int argc, char ** argv) {

    //argument parsing

    if (argc < 3) {
        std::cerr << "At least 2 arguments required" << std::endl;
        show_usage(argv[0]);
        return EXIT_FAILURE;
    }

    std::stringstream sstr;
    for (int i = 1; i < argc; ++i) {
        sstr << argv[i] << " ";
    }
    std::string series_type;
    std::string output_type;

    // default values for optional parameters
    int freq = 1;
    int maxiter = 100;
    int precision = 5;
    std::string sep = " ";
    std::string filename = "output";

    // read positional arguments
    sstr >> series_type >> output_type;

    if ((series_type == "--help") || (series_type == "-h")){ //if first argument is in fact --help
        show_usage(argv[0]);
        return EXIT_SUCCESS;
    }

    // read optional arguments
    std::string argname;
    std::string value;
    while(sstr >> argname >> value) {
        if ((argname == "--freq") || (argname == "-f")){
            freq = std::stoi(value); //add error management
        }
        else if ((argname == "--maxiter") || (argname == "-m")) {
            maxiter = std::stoi(value);
        }
        else if ((argname == "--separator") || (argname == "-s")) {
            sep = value;
        }
        else if ((argname == "--filename") || (argname == "-f")) {
            filename = value;
        }
        else if ((argname == "--precision") || (argname == "-p")) {
            precision = std::stoi(value);
        }
        else {
            show_usage(argv[0]);
            return EXIT_FAILURE;
        }
    }
    // create instances
    std::unique_ptr<Series> S = nullptr;
    if (series_type == "arithmetic"){
        S = std::make_unique<ComputeArithmetic>();
        std::cout << "Computing arithmetic series\n";
    }
    else if (series_type == "pi"){
        S = std::make_unique<ComputePi>();
        std::cout << "Computing pi approximation\n";
    }
    else {
        std::cerr << "please specify a valid series type (arithmetic or pi)" << std::endl;
        return EXIT_FAILURE;
    }

    std::unique_ptr<DumperSeries> D = nullptr;
    if (output_type == "print"){
        D = std::make_unique<PrintSeries>(*S, freq, maxiter, filename, precision);
        std::cout << "Printing series\n";
    }
    else if (output_type == "write"){
        D = std::make_unique<WriteSeries>(*S, freq, maxiter, sep, filename, precision);
        std::cout << "Writing series to file\n";
    }
    else {
        std::cerr << "please specify a valid output type (print or write)" << std::endl;
        return EXIT_FAILURE;
    }

    // compute series and dump
    std::shared_ptr<std::ofstream> os = D->getOutputStream();
    *os << *D; //any ostream can be used here instead. Example: std::cout << *D;
    return EXIT_SUCCESS;
}