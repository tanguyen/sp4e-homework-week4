#include "print_series.hh"


//constructor

PrintSeries::PrintSeries(Series & series, int freq, int maxiter, std::string filename, unsigned int precision)
            : DumperSeries(series, freq, maxiter,filename, precision) {
    this->setFilename(filename); //includes verification of filename
}


//destructor
PrintSeries::~PrintSeries() = default;

std::shared_ptr<std::stringstream> PrintSeries::getOutputString(){
    std::shared_ptr<std::stringstream>  s = std::make_shared<std::stringstream>();
    s->precision(this->precision);
    double pred =  this->series.getAnalyticPrediction();
    *s << "Value ";
    *s << (std::isnan(pred) ? "\n" : "\tConvergence\n");
    for (int i=1; i<this->maxiter/this->freq; i++){
        double res = this->series.compute(i*this->freq);
        *s << res;
        std::isnan(pred) ? *s <<  "\n" : *s << "\t" << pred-res << "\n";
    }
    return s;
}

void PrintSeries::setFilename(std::string filename) {
    // check if file has extension and adds ".txt" extension if not
    std::string name = filename.substr(0, filename.find("."));
    std::string extension = filename.substr(0, filename.find(".") + 1);
    if (name.empty()){
        std::cout << "filename not valid, using 'output' instead" << std::endl;
        name = "output";
    }
    if (extension.empty()){
        std::cout << "'.txt' extension added to filename" << std::endl;
        extension = "txt";
    }
    this->filename = name + "." + extension;
}