#ifndef COMPUTE_ARITHMETIC_HH
#define COMPUTE_ARITHMETIC_HH
#include "series.hh"

class ComputeArithmetic : public Series{
public:
    ComputeArithmetic(); // Constructor
    virtual ~ComputeArithmetic(); // Destructor

    double computeTerm(unsigned int k) override;
};

#endif