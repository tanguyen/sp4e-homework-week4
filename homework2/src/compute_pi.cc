#include "compute_pi.hh"

ComputePi::ComputePi() = default;
ComputePi::~ComputePi() = default;

double ComputePi::computeTerm(unsigned int k) {
    return 1./double(k*k);
}

double ComputePi::compute(unsigned int N) {
    Series::compute(N);
    return sqrt(6.*this->current_value);
}

double ComputePi::getAnalyticPrediction() {
    return M_PI;
}