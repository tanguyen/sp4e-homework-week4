#include "write_series.hh"


//constructor
WriteSeries::WriteSeries(Series &series, int freq, int maxiter, std::string sep,
                         std::string filename, unsigned int precision)
            :DumperSeries(series, freq, maxiter,filename,precision) {
    this->setSeparator(sep); //this->filename is checked and assigned here
}

//destructor
WriteSeries::~WriteSeries() = default;

std::shared_ptr<std::stringstream> WriteSeries::getOutputString(){

    std::shared_ptr<std::stringstream>  s = std::make_shared<std::stringstream>();
    //create one stringstream for each row
    std::stringstream values_row;
    values_row << "Value" << this->sep;
    std::stringstream convergence_row;
    convergence_row << "Convergence" << this->sep;
    //set precision
    values_row.precision(this->precision);
    convergence_row.precision(this->precision);
    //get values
    double pred =  this->series.getAnalyticPrediction();
    for (int i=1; i<this->maxiter/this->freq; i++){
        double res = this->series.compute(i*this->freq);
        values_row << res << this->sep;
        if (not std::isnan(pred)) {
            convergence_row << pred - res << this->sep;
        }
    }
    *s << values_row.rdbuf() << "\n";
    if (!isnan(pred)) {
        *s << convergence_row.rdbuf() << "\n";
    }
    return s;
}


void WriteSeries::setSeparator(std::string sep) {
    // check if separator is valid and standardize the string
    std::unordered_set<std::string> s = {" ", "space", "\t", "tab",
                                         ",", "comma",
                                         "|", "pipe"};
    if (s.find(sep) != s.end()) {
        if (sep == "space") {this->sep = " ";}
        else if (sep == "tab") {this->sep = "\t";}
        else if (sep == "comma") {this->sep = ",";}
        else if (sep == "pipe") {this->sep = "|";}
        else {this->sep = sep;}
    }
    else { //separator not valid
        std::cout << "Separator not valid (should be space or , or |), using space instead" << std::endl;
        this->sep = " ";
    }
    this->setFilename(this->filename);
}


void WriteSeries::setFilename(std::string filename) {
    // remove extension because it will be determined automatically
    std::string name = filename.substr(0, filename.find("."));
    if (name.empty()){
        std::cout << "filename not valid, using name 'output' instead" << std::endl;
        name = "result";
    }

    //add the correct extension
    std::string ext;
    if (this->sep == " " || this->sep == "\t") {ext = "txt";}
    else if (this->sep == ",") {ext = "csv";}
    else if (this->sep == "|") {ext = "psv";}

    this->filename = name + "." + ext;
}
