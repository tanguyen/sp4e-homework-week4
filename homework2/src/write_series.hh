#ifndef WRITE_SERIES_HH
#define WRITE_SERIES_HH

#include "dumper_series.hh"
#include <unordered_set>

class WriteSeries : public DumperSeries {

public:
    //Constructor
    WriteSeries(Series &series, int freq = 1, int maxiter = 100,
                std::string sep = " ", std::string filename = "output", unsigned int precision = 5); // Constructor
    // Destructor
    virtual ~WriteSeries();

    void setSeparator(std::string sep);
    void setFilename(std::string filename) override;
    std::shared_ptr<std::stringstream> getOutputString() override;

private:
    std::string sep;
};

#endif