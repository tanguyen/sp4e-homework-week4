#include "series.hh"
#include <cmath>

Series::Series() = default;
Series::~Series() = default;

double Series::compute(unsigned int N) {
    if (this->current_index < N){
        N -= this->current_index;
    }
    else {
        this->current_value = 0.;
        this->current_index = 0;
    }
    for (unsigned int i=0; i<N; i++){
        this->add_term();
    }
    return this->current_value;
}

void Series::add_term(){
    this->current_index++;
    this->current_value += this->computeTerm(this->current_index);
}

double Series::getAnalyticPrediction(){
    return nan("");
}