#include "dumper_series.hh"
#include "series.hh"
#include <iostream>

// Constructor
DumperSeries::DumperSeries(Series &series, int freq, int maxiter,
                           std::string filename, unsigned int precision): series(series) {
    this->maxiter = maxiter;
    this->freq = freq;
    this->precision = precision;
    this->filename = filename;
}

//Destructor 
DumperSeries::~DumperSeries()= default;

void DumperSeries::dump(std::ostream & os){
    std::shared_ptr<std::stringstream> s;
    s = this->getOutputString();
    os << s->str();
}

void DumperSeries::setPrecision(unsigned int precision){
    this->precision = precision;
};

std::shared_ptr<std::ofstream> DumperSeries::getOutputStream(){
    //setting output file stream
    char *output_file;
    output_file = &(this->filename)[0]; //convert string to char
    std::shared_ptr<std::ofstream> fout = std::make_unique<std::ofstream>(output_file);
    std::cout << "Output will be written to file " << output_file << std::endl;
    //set precision
    fout->precision(this->precision);
    return fout;
}







