#ifndef SERIES_HH
#define SERIES_HH

#include <cmath>
#include <iostream>

class Series{
public:
    // Constructor
    Series();
    // Destructor
    virtual ~Series();

public:
    virtual double compute(unsigned int N);
    void add_term();
    virtual double computeTerm(unsigned int k) = 0;
    virtual double getAnalyticPrediction();

    unsigned int current_index = 0;
    double current_value = 0.;
};

#endif