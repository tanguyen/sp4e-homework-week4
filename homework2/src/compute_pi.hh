#ifndef COMPUTE_PI_HH
#define COMPUTE_PI_HH

#include "series.hh"

class ComputePi : public Series{
public:
    ComputePi(); // Constructor
    virtual ~ComputePi(); // Destructor

    double compute(unsigned int N) override;
    double computeTerm(unsigned int k) override;
    double getAnalyticPrediction() override;
};

#endif