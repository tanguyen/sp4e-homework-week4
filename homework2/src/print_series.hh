#ifndef PRINT_SERIES_HH
#define PRINT_SERIES_HH


#include "dumper_series.hh"

class PrintSeries : public DumperSeries {

public:
    PrintSeries(Series & series, int freq, int maxiter,
                std::string filename = "output", unsigned int precision = 5); // Constructor
    virtual ~PrintSeries(); // Destructor

    void setFilename(std::string filename) override;
    std::shared_ptr<std::stringstream> getOutputString() override;

private: 

}; 


#endif