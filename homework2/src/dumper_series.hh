#ifndef DUMPER_SERIES_HH
#define DUMPER_SERIES_HH

#include "series.hh"
#include "compute_arithmetic.hh"
#include "compute_pi.hh"

#include <string>
#include <iostream>
#include <cmath>
#include <fstream>
#include <sstream>


class DumperSeries{

public:

    //Constructor
    DumperSeries(Series & series, int freq = 1, int maxiter = 100,
                 std::string filename="output", unsigned int precision = 5);
    // Destructor
    virtual ~DumperSeries();

    void setPrecision(unsigned int precision);
    virtual void setFilename(std::string filename) = 0;
    virtual std::shared_ptr<std::stringstream> getOutputString() = 0;
    std::shared_ptr<std::ofstream> getOutputStream();

    void dump(std::ostream & os); // = 0;

protected:

    Series &series;
    int maxiter;
    int freq;
    unsigned int precision;
    std::string filename = "output";

};

// outstream operator
inline std::ostream & operator <<(std::ostream & stream, DumperSeries & _this){
    _this.dump(stream);
    return stream;
}

#endif

