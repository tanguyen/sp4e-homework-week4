import argparse 
import numpy as np 
import matplotlib.pyplot as plt
#import os
#import os.path


def plot1(x,y):
    # Figure object
    fig = plt.figure()
    # Axe object
    axe = fig.add_subplot(111)
    axe.plot(x,y)
    plt.show()

def plot2(x,y1,y2):
	plt.plot(x,y1)
	plt.plot(x,y2)
	plt.show()


def main():

    #Parse arguments
    parser = argparse.ArgumentParser(
             description='Reads series file and create a plot')
    
    parser.add_argument('filename', type=str, required=True,
                         help='specify name of file containing series output')
    parser.add_argument('--path', type=str, default = "./src/build/",
                         help='specify the path to the file')
    parser.add_argument('-e','--extension', type=str, default="txt",
                         help='specify the file extension. By default: txt')
    args = parser.parse_args()
    
    #log.basicConfig(level=args.loglevel)
    
    filename = args.filename
    ext = args.ext 
    
    #define separator 
    if ext == "txt":
       sep = ' ' 
    elif ext == "csv":
       sep = ','
    elif ext == "psv":
       line = '|'

    #open file 
    with open(filename+"."+ext, "r") as file:
         line = file.readline().split(sep)
         second_line = file.readline().split(sep)
        
    line.pop(0) #remove 1st term ("value")
    line = [float(item) for item in line] #convert list of string to float 
    y1 = np.array(line)
    
    if len(second_line) == len(line)-1: #check if second line contains something
	second_line.pop(0)
	second_line = [float(item) for item in second_line]
	y2 = np.array(second_line)
   
    
    x = np.arange(0,len(y))
    
    if 'y2' in locals():
	plot2(x,y1,y2)
    else:
	plot1(x,y1)
    

if __name__ == '__main__': 
    main()
    
    


