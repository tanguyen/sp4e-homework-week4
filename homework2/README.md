sp4e Homework 2 (october 2020)

Team members:
Thien-Anh Nguyen
Roman Pohorsky

Work repartition:
To start the project, we decided to divide the work per class family: one team member works on the Series family (Series, ComputeArithmetic, ComputePi), the other team member works on the DumperSeries family (DumperSeries, PrintSeries, WriteSeries).
When these two family were implemented we could split the next tasks easily.


How to execute code:

- Build the project
- Then, execute main using:

Usage: ./.../main series_type output_type <option(s)>
	 series_type 	 'pi' or 'arithmetic'
	 output_type 	 'print' or 'write' (i.e write to file)
Options:
	-h,--help 	Show this help message
	-f,--freq 	Frequency of the output. Default: 1
	-m,--maxiter 	Maximum number of terms in the series. Default: 100
	-s,--separator 	Separator used when writing the output.
			Choose among {" ", "space", "\t", "tab",
			",", "comma","|", "pipe"}
			Default: " "
	-f,--filename 	Name of file where output will be printed/written.
	-p,--precision 	Precision of values to be printed/written.
			Extension determined automatically based on separator.
			Default: 'output'

Example: from the executable location, use command:
./main pi write --maxiter 100 --freq 10 --filename results --precision 5 --separator ","

Remarks:
The difference between printing to a file and writing to a file are:
	- print: values written in a column, write: values written in a row
	- write will choose the output file type based on the separator (space or tab -> 	txt, comma -> csv, pipe -> psv). print will always use space as the separator.

Plotting results:
A plotSeries.py script is located in the homework2 repository. It will plot the results from the main script if the write option was chosen. 
To launch it, you need to parse the file name (without the extension). If the extension is different from txt, you will have to add it as a second argument.

Example: from the homework2 repository, type:
ex 1) python3 plotSeries.py filename1 
ex 2) python3 plotSeries.py filename2 csv


