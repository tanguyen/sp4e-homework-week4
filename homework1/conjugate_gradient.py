#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 13:32:00 2020

@author: tanguyen
"""
import numpy as np

def my_CG_minimizer(A,b,tol,x):
    """
    Custom implementation of the Conjugate Gradient algorithm.
    Finds x minimizing 1/2 (xT)Ax - (xT)b

    Parameters
    ----------
    A : np array
        Matrix A.
    b : np array
        Vector b.
    tol : float
        tolerance.
    x : np array
        initial guess.

    Returns
    -------
    x : np array
        result.
    i : int
        number of iterations.
    x_list : list
        list of results for each iteration.

    """
    r = b - np.einsum('ik,k->i', A, x)
    rnorm = np.einsum('k,k->', r, r)
    p = r
    
    x_list = [x]
    
    for i in range(b.shape[0]):
        Ap = np.einsum('ik,k->i', A, p)
        alpha = rnorm / np.einsum('k,k->', p.T, Ap)
        x = x + alpha * p
        r = r - alpha * Ap
        rnorm_new = np.einsum('k,k->', r, r)
        x_list.append(x)
        
        if np.sqrt(rnorm) < tol:
            break
        p = r + (rnorm_new / rnorm) * p
        rnorm = rnorm_new
        i += 1
        
    return x, i, x_list



