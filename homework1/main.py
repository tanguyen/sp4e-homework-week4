# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 12:29:19 2020

@author: roman
"""
import argparse
import numpy as np
from conjugate_gradient import my_CG_minimizer
from optimizer import scipy_minimizer
from miscellaneous import optplot, create_fun
import logging as log

def main():

    #Parse arguments
    parser = argparse.ArgumentParser(
            description='Finds x minimizing 1/2 (xT)Ax - (xT)b, and plots the result')
    
    parser.add_argument('Afilename', type=str,
                        help='symmetric positive semi-definite matrix of size (n,n), \
                        specify name of file containing coefficients of A')
    parser.add_argument('bfilename', type=str,
                        help='vector of size (n,1), \
                        specify name of file containing coefficients of b')
    parser.add_argument('--tol', type=float, default=1e-5,
                        help='tolerance')
    parser.add_argument('--impl', type=str, default='scipy',
                        help ='specify implementation, \'custom\' or \'scipy\'')
    parser.add_argument('--method', type=str, default='CG',
                        help ='specify minimization method, \'CG\' or \'BFGS\'')
    parser.add_argument('--verbose', action="store_const", dest="loglevel", \
                        const=log.DEBUG, default=log.INFO, \
                        help='will increase verbosity')
    parser.add_argument('--plot', action='store_true', 
                        help='plot the process')
    args = parser.parse_args()
    
    log.basicConfig(level=args.loglevel)

        
    #Load coefficients from files
    A = np.loadtxt(args.Afilename) 
    b = np.loadtxt(args.bfilename)
    #check dimensions
    dim = A.shape[0]
    if A.shape != (dim, dim) or b.shape != (dim,):
        raise ValueError("Please check the dimensions of A and b")
    if (not np.all(np.abs(A-A.T) < 1e-8)) and args.method=='CG':
        raise ValueError("To use the conjugate gradient method, A should be a symmetric matrix ")
                                 
    #create functor
    s = create_fun(A, b)
    
    #Initialization
    x = np.ones(b.shape)*2
    
    #Proceed to minimization
    log.debug('Starting minimization...')
    if args.impl == 'custom':
        if args.method == 'CG':
            x, niter, x_list = my_CG_minimizer(A,b,args.tol,x) 
        else: 
            raise ValueError('Custom implementation not available for the specified method')
    elif args.impl == 'scipy':
        x, niter, x_list = scipy_minimizer(s,args.tol,x,args.method)
    else:
        raise ValueError('Please specify a valid implementation type')
    log.debug('Done.')
        
    #Display result
    log.info('Result: {0}'.format(x))
    log.info('Number of iterations: {0}'.format(niter))
    log.debug('Successive values of x: {0}'.format(x_list))
    #plot result    
    if args.plot:
        if dim != 2:
            log.warning('Plotting available for dimension=2 only')
        else:
            optplot(s, x_list, args.method)
        
        
if __name__ == '__main__':
    main()   
