# -*- coding: utf-8 -*-
"""
Created on Thu Oct 8 13:27:21 2020

@author: roman
"""

from scipy import optimize
import numpy as np

def scipy_minimizer(fun,tol,initial_guess,method = 'CG'):
    """Calls the scipy.optimize.minimize.function -> Minimization of scalar function of one or more variables
       Stores the result of each iteration into a list x_list.
    
    Parameters:
    ----------- 
        fun : 	callable
        	The function to be minimized
        
        initial_guess : list, array of real elements for each independent variable
        
        method : string. default = 'CG'
        
	Can also be: 
            - 'CG'          
            - 'BFGS'       
          

    Returns:
    --------
        result.x: np array, the minimization result (2D vector)
        
        result.nit: int, number if iterations
        
        x_list: list, list of results of each iteration
    """
    
    #create list to store successive values of X
    x_list = [np.array(initial_guess).T]
    
    #define callback function to store X at each iteration
    def callbackfun(x):
        nonlocal x_list
        x_list.append(x)
                
    result = optimize.minimize(fun, initial_guess, method = method, tol=tol, callback=callbackfun)
    
    if result.success:
     return result.x, result.nit, x_list
    else:
     raise ValueError(result.message)


