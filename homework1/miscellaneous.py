# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 17:39:38 2020

@author: roman
"""


import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import logging


def optplot(s, x_list, method):
    """
    Plots function S and the result of each iteration of the minimization process

    Parameters
    ----------
    s : callable function
        function to minimize.
    x_list : list
        list of 2D coordinates corresponding to the iterations.
    method : string
        name of the method (used as a title for the figure).

    Returns
    -------
    None.

    """
    logging.getLogger('matplotlib').setLevel(logging.WARNING)
    fig = plt.figure()
    ax = Axes3D(fig)
    
    #plot the result of each iteration
    Xopt = [x[0] for x in x_list]
    Yopt = [x[1] for x in x_list]
    Sopt = [s(x) for x in x_list]
        
    ax.plot(Xopt, Yopt, Sopt, 'ro--')

    #surface plot of S
    X = np.arange(-3, 3, 0.25)
    Y = np.arange(-3, 3, 0.25)
    X, Y = np.meshgrid(X, Y)

    #apply function S to all point of grid
    coord = np.dstack((X,Y))
    S = np.array([[s(x) for x in coord[row]] for row in range(len(X))])
    
    # Plot the surface.
    zmax = 70
    ax.plot_surface(X, Y, S, cmap=cm.coolwarm, alpha=0.5,
                       linewidth=0, antialiased=False, vmax = zmax)
    ax.contour3D(X, Y, S, 25, cmap='binary')
    
    # Customize the z axis.
    ax.set_zlim(0, zmax)
    
    # add axis labels
    ax.set_xlabel("X axis")
    ax.set_ylabel("Y axis")
    ax.set_zlabel("Z axis")
    
    ax.view_init(elev=50, azim=160)
    
    ax.set_title(method)
    
    plt.show()
    
    
def create_fun(A, b):
    """
    Creates a lambda function for S

    Parameters
    ----------
    A : np array
        Matrix A.
    b : np array
        Vector b.

    Returns
    -------
    lambda function
        computes 0.5*(X.T@A)@X - X.T@b.

    """
    return lambda X: 0.5*(X.T@A)@X - X.T@b